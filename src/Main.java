import java.util.Scanner;

/**
 * Ciaran Vinken
 * 24/09/2020
 */
public class Main {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int aantalGebruikers = 0;
        int aantalVerslaving = 0;
        while (true) {
            System.out.println();
            System.out.println("Geef uw gebruikersnaam in: ");
            String uname = kb.nextLine();
            aantalGebruikers++;
            int teller = 0;

            System.out.println("Het laatste wat ik doe -voor ’s avonds het licht uit gaat- is nog snel even kijken of er nog iets aan berichten, enz is binnengekomen op mijn smartphone.");
            System.out.println("Antwoord: ");
            String reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;

            System.out.println("Het eerste wat ik doe -al ’s morgens het licht aangaat- is kijken of er berichten, enz zijn binnengekomen op mijn smartphone.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;

            System.out.println("Ik heb notificaties (die een geluidje/of een trilbeweging maken) geactiveerd op mijn smartphone.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) {
                teller++;
            }

            System.out.println("Wanneer ik merk dat er een notificatie op mijn smartphone is binnen gekomen, kijk ik binnen de minuut op mijn smartphone wat het precies is.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) {
                teller++;
            }

            System.out.println("Terwijl is naar een tv-reeks of film kijk op TV of computerscherm, gebruik ik één of meerdere keren mijn smartphone.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;

            System.out.println("Als ik alleen eet (ontbijt/lunch/diner) gebruik ik meermaals mijn smartphone tijdens de maaltijd.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;

            System.out.println("Als ik in gezelschap -familie, vrienden,… eet (ontbijt/lunch/diner) gebruik ik meermaals mijn smartphone tijdens de maaltijd.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;

            System.out.println("Als ik sta te wachten (op een bus, trein, tram, een vriend(in),..), gebruik ik mijn smartphone.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;

            System.out.println("Als in -alleen- op straat loop doe ik dat met mijn smartphone in de hand en kijk ik geregeld naar het scherm.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;

            System.out.println("Als ik fiets of met de auto rij, durf ik wel eens de smartphone in de hand te nemen en naar het scherm te kijken.");
            System.out.println("Antwoord: ");
            reply = kb.nextLine();
            if (reply.equalsIgnoreCase("ja")) teller++;



            System.out.println("resultaat: ");
            if (teller == 0) {
                System.out.println(uname + ", Je bent niet digitaal verslaafd. Houden zo!");
            } else if (teller >= 1 && teller <= 2) {
                System.out.println(uname + ", Je hebt een milde vorm van digitale verslaving.");
            } else if (teller >= 3 && teller <= 5) {
                System.out.println(uname + ", Je hebt een niet te onderschatten vorm van digitale verslaving. Leg jezelf wat beperking op.");
            } else if (teller >= 6 && teller <= 8) {
                System.out.println(uname + ", Je hebt een ernstige vorm van digitale verslaving. Overweeg om een digitale detox-cursus te volgen!");
                aantalVerslaving++;
            } else {
                System.out.println(uname + ", Je hebt een extreme vorm van digitale verslaving. Jouw verslaving vormt een gevaar voor jezelf en jouw omgeving. Zoek zo snel mogelijk hulp!!");
                aantalVerslaving++;
            }
            System.out.println("Aantal gebruikers die al deelgenomen hebben: " + aantalGebruikers);
            System.out.println("Aantal gebruikers die ernstige of extreme vorm van digitale verslaving hebben: " + aantalVerslaving);
        }
    }
}
